# README #


### Checklist: ###

1. Programming language: Swift 4.2.1, xCode 10.1
2. Design app's architecture/patterns: MVVM, Singleton, Delegation.
3. UI looks like the desgin.
4. Exception handling.
5. Caching handling.
6. Accessibility for Disability Supports: VoiceOver, Scaling Text. 
7. Entity for database.

### Some explanations for the project: ###
* Caching handing:
	1. Use CoreData:
	
		a. To store the data locally.
		
		b. Retrieve the local data in case of user inputs one of previous inputed key searchs.
		
		
	2. Once user presses the search button:
	
		a. Retrieve the local data to prevent the app from generating many API requests.
		
		b. If there is no local data -> the API will be called. 
		
		
	3. The local data is stored following the number of forecast days (for example 7 days), when the local data is obsolete (after 7 days), it will be deleted.
	


* Exception handling: 
	1. Network error, parsing data error, server respone error.
    2. An alert will be displayed when there is an exception such as 'city not found'.
	