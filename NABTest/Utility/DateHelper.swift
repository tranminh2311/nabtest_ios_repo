//
//  DateHelper.swift
//  NABTest
//
//  Created by Minh Tran on 3/6/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation
class DateHelper {
    
    class func getDateString(date:Date, timezone:TimeZone, format: String) -> String{
        var calendar = Calendar.current
        calendar.timeZone = timezone
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: date)
    
        return dateString
    }
    
}
