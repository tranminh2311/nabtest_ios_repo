//
//  AlertHelper.swift
//  NABTest
//
//  Created by Minh Tran on 3/6/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//
import UIKit
import Foundation

class AlertHelper {
    
    class func showAlert(title:String, message: String, controller: UIViewController?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        controller?.present(alert, animated: true, completion: nil)
    }
}
