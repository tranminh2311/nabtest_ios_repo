//
//  Constants.swift
//  NABTest
//
//  Created by Minh Tran on 3/6/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

let RESPONSE_OK_COD = "200"
let CITY_NOT_FOUND_COD = "404"
let OUT_OF_CNT_COD = "400"

let RESULT_NOT_FOUND = "Result not found"
let SOMETHING_WENT_WRONG = "Something went wrong"
let EMPTY_RESULT = "Empty result"
let RESULT = "Result"
let OOPS = "Oops"

let DATE_STRING_FORMAT = "E, dd MMM yyyy"
let NUMBER_OF_FORECAST_DAYS = 14

