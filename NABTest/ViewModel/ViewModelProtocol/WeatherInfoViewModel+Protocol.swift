//
//  WeatherInfoViewModel+Protocol.swift
//  NABTest
//
//  Created by Minh Tran on 3/6/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

@objc protocol WeatherInfoViewModelDelegate:NSObjectProtocol {
    @objc optional func didFinishGettingWithWeatherInfo()
    @objc optional func didFailGettingDataWithParsingError(error: Error)
    @objc optional func didFinishGettingDataWithEmptyResult()
    @objc optional func didFailGettingDataWithAPIError(error: Error)
}
