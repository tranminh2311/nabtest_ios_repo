//
//  WeatherInfoViewModel.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

class WeatherInfoViewModel {
    
    var delegate:WeatherInfoViewModelDelegate!
    var weatherInfo:WeatherInfo!
    
    func getWeatherInfo(searchKey:String) {
        StorageManager.fetchSavedData(searchKey: searchKey, completion: { (data) in
            if data == nil { // saved data not found
                self.callAPIGettingWeatherInfo(searchKey: searchKey)
            } else {
                self.manipulateDataAndSave(searchKey: searchKey, data: data!, isSave: false)
            }
        })
    }
    
    // MARK: Private methods
    private func manipulateDataAndSave(searchKey:String, data:Data, isSave: Bool){
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            let weather = WeatherInfo.parseObjectFrom(json: json)
            guard weather != nil else {
                self.finishGettingDataWithEmptyResult()
                return
            }
            self.weatherInfo = weather
            self.finishGettingWeatherInfo()
            
            if isSave {
                // save data locally
                let calendar = Calendar.current
                let today = calendar.date(byAdding: .day, value: 0, to: Date()) ?? Date()
                StorageManager.save(searchKey: searchKey, createdDate: today,data: data)
            }
        } catch {
            self.failGettingDataWithParsingError(error: error)
        }
    }
    
    private func callAPIGettingWeatherInfo(searchKey:String){
        APIManager.getWeatherInfo(searchKey: searchKey) { (data, error) in
            if let data = data {
                self.manipulateDataAndSave(searchKey: searchKey, data: data, isSave: true)
            } else {
                if error != nil {
                    self.failGettingDataWithAPIError(error: error!)
                } else {
                    self.finishGettingDataWithEmptyResult()
                }
            }
        }
    }
}

// MARK: WeatherInfoViewModel delegation
// MARK: Private methods
extension WeatherInfoViewModel {
    private func hasDelegation() -> Bool {
        guard self.delegate != nil else {
            return false
        }
        
        return self.delegate.conforms(to: WeatherInfoViewModelDelegate.self)
    }
    
    private func finishGettingWeatherInfo(){
        if self.hasDelegation() {
            if self.delegate.responds(to: #selector(WeatherInfoViewModelDelegate.didFinishGettingWithWeatherInfo)){
                self.delegate.didFinishGettingWithWeatherInfo?()
            }
        }
    }
    
    private func failGettingDataWithParsingError(error: Error){
        if self.hasDelegation() {
            if self.delegate.responds(to: #selector(WeatherInfoViewModelDelegate.didFailGettingDataWithParsingError(error:))){
                self.delegate.didFailGettingDataWithParsingError?(error: error)
            }
        }
    }
    
    private func finishGettingDataWithEmptyResult(){
        if self.hasDelegation() {
            if self.delegate.responds(to: #selector(WeatherInfoViewModelDelegate.didFinishGettingDataWithEmptyResult)){
                self.delegate.didFinishGettingDataWithEmptyResult?()
            }
        }
    }
    
    private func failGettingDataWithAPIError(error: Error){
        if self.hasDelegation() {
            if self.delegate.responds(to: #selector(WeatherInfoViewModelDelegate.didFailGettingDataWithAPIError(error:))){
                self.delegate.didFailGettingDataWithAPIError?(error: error)
            }
        }
    }
}
