//
//  WeatherInfo.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

class WeatherInfo: NSObject {
    var cod:String?
    var message:String?
    var cnt:String?
    var timezone:String?
    var weatherItems:[WeatherItem]?
}
