//
//  WeatherInfo+Parser.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

extension WeatherInfo {
    
    class func parseObjectFrom(json:Dictionary<String, Any>?) -> WeatherInfo? {
        guard let json = json else {
            return nil
        }
        
        let info = WeatherInfo()
        info.cod = json["cod"] as? String
        info.cnt = json["cnt"] as? String
        info.message = json["message"] as? String
        if let city = json["city"] {
            let dict = city as! Dictionary<String, Any>
            info.timezone = String(describing: dict["timezone"] ?? "")
        }
        
        if let list = json["list"] {
            var weatherItems = [WeatherItem]()
            (list as! Array<Any>).forEach { (element) in
                let elementDict = element as! Dictionary<String, Any>
                let item = WeatherItem()
                item.pressure = String(describing:elementDict["pressure"] ?? "")
                item.humidity = String(describing:elementDict["humidity"] ?? "")
                
                if let temp = elementDict["temp"]{
                    let tempDict = temp as! Dictionary<String,Any>
                    if let min = tempDict["min"], let max = tempDict["max"] {
                        let everage = ((min as! NSNumber).intValue + (max as! NSNumber).intValue) / 2
                        item.everageTemperature = String(describing: everage)
                    }
                }
                
                if let weather = elementDict["weather"] {
                    if (weather as! Array<Any>).count > 0 {
                        let weatherDict = (weather as! Array<Any>)[0] as! Dictionary<String,Any>
                        item.weatherDescription = String(describing: weatherDict["description"] ?? "")
                    }
                }
                weatherItems.append(item)
            }
            info.weatherItems = weatherItems
        }
        
        return info
    }
}
