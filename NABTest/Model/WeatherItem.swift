//
//  WeatherItem.swift
//  NABTest
//
//  Created by Minh Tran on 3/6/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

class WeatherItem: NSObject {
    var everageTemperature:String?
    var pressure:String?
    var humidity:String?
    var weatherDescription:String?
}
