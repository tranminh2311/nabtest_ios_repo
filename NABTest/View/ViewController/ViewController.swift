//
//  ViewController.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var weatherForcastLabel: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let cellIndentifier =  "WeatherInfoTableViewCell"
    let viewModel = WeatherInfoViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setup()
        self.setAccessibility()
    }
}

// MARK: Setup, handling UI, getting data
extension ViewController {
    func setup(){
        self.searchBar.delegate = self
        self.searchBar.setShowsCancelButton(true, animated: true)
        
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.register(UINib.init(nibName: self.cellIndentifier, bundle: Bundle.main), forCellReuseIdentifier: self.cellIndentifier)
        
        self.viewModel.delegate = self
        self.hideActivityIndicator(hidden: true, animating: false)
    }
    
    func reloadTableView(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func hideActivityIndicator(hidden: Bool, animating: Bool){
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = hidden
            if animating {
                self.activityIndicator.startAnimating()
            } else {
                self.activityIndicator.stopAnimating()
            }
        }
    }
}

// MARK: Accessibility: VoiceControls, Larger Text
extension ViewController {
    
    func setAccessibility() {
        self.weatherForcastLabel.isAccessibilityElement = true
        self.weatherForcastLabel.accessibilityValue = self.weatherForcastLabel.text
        
        self.weatherForcastLabel.font = .preferredFont(forTextStyle: .body)
        self.weatherForcastLabel.adjustsFontForContentSizeCategory = true
        
        let textField = self.searchBar.value(forKey: "searchField") as? UITextField
        textField?.font = .preferredFont(forTextStyle: .body)
        textField?.adjustsFontForContentSizeCategory = true
        let placeholderLabel  = textField?.value(forKey: "placeholderLabel") as? UILabel
        placeholderLabel?.font = .preferredFont(forTextStyle: .body)
        placeholderLabel?.adjustsFontForContentSizeCategory = true
    }
}

// MARK:  View Model - getting data
extension ViewController{
    func getWeatherInfo(searchKey:String){
        self.viewModel.getWeatherInfo(searchKey: searchKey)
    }
}

// MARK: UITableViewDataSource
extension ViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let weatherInfo = self.viewModel.weatherInfo, let list = weatherInfo.weatherItems {
            return list.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIndentifier, for: indexPath) as! WeatherInfoTableViewCell
        cell.setWeatherInfo(weatherInfo: self.viewModel.weatherInfo, itemIndex: indexPath.row)
        
        return cell
    }
}

// MARK: UISearchBarDelegate
extension ViewController:UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        guard self.searchBar.text != nil else {
            return
        }
        let searchKey = self.searchBar.text!.trimmingCharacters(in: .whitespaces)
        if searchKey.count >= 3 {
            self.searchBar.resignFirstResponder()
            self.hideActivityIndicator(hidden: false, animating: true)
            self.getWeatherInfo(searchKey: searchKey)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.searchBar.resignFirstResponder()
    }
}

// MARK: WeatherInfoViewModelDelegate
extension ViewController:WeatherInfoViewModelDelegate {
    
    func didFinishGettingWithWeatherInfo() {
        self.hideActivityIndicator(hidden: true, animating: false)
        if let cod = self.viewModel.weatherInfo.cod {
            if cod != RESPONSE_OK_COD {
                if let message = self.viewModel.weatherInfo.message {
                    AlertHelper.showAlert(title: RESULT, message: message, controller: self)
                } else {
                    AlertHelper.showAlert(title: RESULT, message: RESULT_NOT_FOUND, controller: self)
                }
            }
        }
        self.reloadTableView()
    }

    func didFailGettingDataWithParsingError(error: Error) {
        self.hideActivityIndicator(hidden: true, animating: false)
        AlertHelper.showAlert(title: OOPS, message: SOMETHING_WENT_WRONG, controller: self)
    }

    func didFinishGettingDataWithEmptyResult() {
        self.hideActivityIndicator(hidden: true, animating: false)
        AlertHelper.showAlert(title: RESULT, message: EMPTY_RESULT, controller: self)
    }

    func didFailGettingDataWithAPIError(error: Error) {
        self.hideActivityIndicator(hidden: true, animating: false)
        AlertHelper.showAlert(title: OOPS, message: SOMETHING_WENT_WRONG, controller: self)
    }
    
}
