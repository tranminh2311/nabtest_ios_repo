//
//  WeatherInfoTableViewCell.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import UIKit

class WeatherInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var averageTempLabel: UILabel!
    @IBOutlet weak var pressureLable: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupVoiceOverControls()
        self.setAdjustsFontForContentSize()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: Set data
    func setWeatherInfo(weatherInfo:WeatherInfo, itemIndex:Int){ 
        var timezone = TimeZone.current
        if let timezoneStr = weatherInfo.timezone {
            timezone = TimeZone(secondsFromGMT: Int(timezoneStr)!) ?? .current
        }
        var calendar = Calendar.current
        calendar.timeZone = timezone
        let date = calendar.date(byAdding: .day, value: itemIndex, to: Date()) ?? Date()
        let dateString = DateHelper.getDateString(date: date, timezone: timezone, format: DATE_STRING_FORMAT)
        self.dateLabel.text = String(format: "Date: %@", dateString)
        
        guard weatherInfo.weatherItems != nil else {
            return
        }
        let weatherItem = weatherInfo.weatherItems![itemIndex];
        self.averageTempLabel.text = String(format: "Average temperature: %@°C", weatherItem.everageTemperature ?? "")
        self.pressureLable.text = String(format: "Pressure: %@", weatherItem.pressure ?? "")
        self.humidityLabel.text = String(format: "Humidity: %@%%", weatherItem.humidity ?? "")
        self.descriptionLabel.text = String(format: "Description: %@", weatherItem.weatherDescription ?? "")
        
        self.setVoiceOverControlsValues()
    }
}

// MARK: Accessibility: VoiceControls, Larger Text
extension WeatherInfoTableViewCell {
    
    // MARK: Private methods
    private func setupVoiceOverControls() {
        self.dateLabel.isAccessibilityElement = true
        self.averageTempLabel.isAccessibilityElement = true
        self.pressureLable.isAccessibilityElement = true
        self.humidityLabel.isAccessibilityElement = true
        self.descriptionLabel.isAccessibilityElement = true
    }
    
    private func setVoiceOverControlsValues() {
        self.dateLabel.accessibilityValue = self.dateLabel.text
        self.averageTempLabel.accessibilityValue = self.averageTempLabel.text
        self.pressureLable.accessibilityValue = self.pressureLable.text
        self.humidityLabel.accessibilityValue = self.humidityLabel.text
        self.descriptionLabel.accessibilityValue = self.descriptionLabel.text
    }
    
    private func setAdjustsFontForContentSize() {
        self.dateLabel.font = .preferredFont(forTextStyle: .body)
        self.dateLabel.adjustsFontForContentSizeCategory = true
        self.averageTempLabel.font = .preferredFont(forTextStyle: .body)
        self.averageTempLabel.adjustsFontForContentSizeCategory = true
        self.pressureLable.font = .preferredFont(forTextStyle: .body)
        self.pressureLable.adjustsFontForContentSizeCategory = true
        self.humidityLabel.font = .preferredFont(forTextStyle: .body)
        self.humidityLabel.adjustsFontForContentSizeCategory = true
        self.descriptionLabel.font = .preferredFont(forTextStyle: .body)
        self.descriptionLabel.adjustsFontForContentSizeCategory = true
    }
}
