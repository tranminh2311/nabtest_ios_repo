//
//  APIManager.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation

class APIManager {
    
    class func getWeatherInfo(searchKey:String, completion: @escaping (Data?,Error?) -> ()){
        let urlString = String(format: "https://api.openweathermap.org/data/2.5/forecast/daily?q=%@&cnt=%@&appid=60c6fbeb4b93ac653c492ba806fc346d&units=metric",searchKey,String(NUMBER_OF_FORECAST_DAYS))
        let escapedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let url = URL(string: escapedString!)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 30
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            completion(data,error)
        }
        task.resume()
    }
}
