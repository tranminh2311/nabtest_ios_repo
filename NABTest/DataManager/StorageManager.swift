//
//  StorageManager.swift
//  NABTest
//
//  Created by Minh Tran on 3/4/21.
//  Copyright © 2021 Minh Tran. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class StorageManager {
    
    class func defaultManagedContext(completion: @escaping (NSManagedObjectContext?) -> ()){
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            guard appDelegate != nil else {
                completion(nil)
                return
            }
            let managedContext = appDelegate!.persistentContainer.viewContext
            completion(managedContext)
        }
    }
    
    class func save(searchKey: String, createdDate: Date, data: Data) {
        defaultManagedContext { (managedContext) in
            guard let managedContext = managedContext else {
                return
            }
            let entity = NSEntityDescription.entity(forEntityName: "WeatherInfoData",
                                           in: managedContext)!
            let weatherInfoData = NSManagedObject(entity: entity,
                                                  insertInto: managedContext)
            weatherInfoData.setValue(searchKey, forKeyPath: "searchKey")
            weatherInfoData.setValue(data, forKeyPath: "data")
            weatherInfoData.setValue(createdDate, forKeyPath: "createdDate")
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    class func fetchSavedData(searchKey:String, completion:@escaping (Data?) -> ()){
        defaultManagedContext { (managedContext) in
            guard let managedContext = managedContext else {
                completion(nil)
                return
            }
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "WeatherInfoData")
            do {
                let objects: [NSManagedObject] = try managedContext.fetch(fetchRequest)
                var data:Data?
                var dataAvailable = false
                for item in objects {
                    let key = item.value(forKey: "searchKey") as? String
                    if let key = key {
                        if key == searchKey {
                            data = item.value(forKey: "data") as? Data
                            let createdDate = item.value(forKey: "createdDate") as? Date
                            if let createdDate = createdDate {
                                let calendar = Calendar.current
                                let availableFromDate = calendar.date(byAdding: .day, value: -NUMBER_OF_FORECAST_DAYS, to: Date()) ?? Date()
                                let compareResult = calendar.compare(createdDate, to: availableFromDate, toGranularity: .day)
                                if compareResult == .orderedDescending {
                                    // data is available
                                    dataAvailable = true
                                    break
                                } else {
                                    // data is out of date -> should be deleted
                                    managedContext.delete(item)
                                    try managedContext.save()
                                }
                            }
                        }
                    }
                }
                
                if dataAvailable {
                    completion(data)
                } else {
                    completion(nil)
                }
            } catch let error as NSError {
                print("Could not fetch. \(error), \(error.userInfo)")
                completion(nil)
            }
        }
    }
}
